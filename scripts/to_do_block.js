
var curPage, pageCount, curType;

var toDo = {};

toDo.decodeHTMLEntities = function(text)
{
	var ta=document.createElement("textarea");
	ta.innerHTML=text.replace(/</g,"&lt;").replace(/>/g,"&gt;");
	return ta.value;
}

toDo.buildLink = function(pageNumber, text, type)
{
	return "<a href=\"" + toDo.URLbase + "to_do/json/" + type + "/" + pageNumber + "\">" + text + "</a>";
}

toDo.pagination = function(currentPage, tabPages, type)
{
	currentPage = Number(currentPage)
	var output = "";
	if(tabPages > 1)
	{
		output += (currentPage != 1) ?  toDo.buildLink(currentPage - 1, "<", type) : "<span class=\"pagination_arrow\"><</span>";
		for(var i = 1; i <= tabPages; i++)
		{
			if(i == currentPage)
			{
				output += " <span class=\"current_pagination\">" + i + "</span>";
			}
			else if(i == 1  || i == (currentPage - 1) || i == (currentPage + 1) || i == tabPages)
			{
				output += " ";
				output += toDo.buildLink(i, i, type);
			}
			else if(i == currentPage + 2 || i == currentPage - 2)
			{
				output += " ...";
			}
		}
		output += (currentPage != tabPages) ? toDo.buildLink(currentPage + 1, " >", type) : " <span class=\"pagination_arrow\">></span>";

	}
	return output;
}

toDo.setTabHandlers = function()
{
	$("#block-to_do-0 ul li a").each(function()
	{
		$(this).click(function()
		{
			var changeTabClass = false;
			if(($(this).attr("href")).search(curType) < 0)
			{
				changeTabClass = true;
				curType = (curType == "immediate") ? "all" : "immediate";
			}
			pageCount = (curType == "immediate") ? immediatePageCount : allPageCount;
			toDo.action($(this), true, changeTabClass);
			return false;
		});
	});
}

toDo.setPaginationHandlers = function()
{
	$("#to_do_pagination a").each(function()
	{
		$(this).click(function()
		{
			toDo.action($(this));
			return false;
		});
	});
}

toDo.action = function(e, tabs, changeTabClass)
{
	var newHref = e.attr("href");
	var pieces = newHref.split("/");
	var curPage = pieces[pieces.length - 1];
	toDo.getData(newHref, curPage, tabs, changeTabClass);
}

toDo.getData = function(targetHref, currentPage, tabs, changeTabClass)
{
	$.getJSON(targetHref, null, function(data)
	{
		var output = '<tbody>';
		var priorityExists = false;
		$.each(data.items, function(i, items)
		{
			if(items.itm)
			{
				output += "<tr";
				if(Number(items.itm.priority))
				{
					priorityExists = true;
					output += " class=\"immediate\"";
				}
				output += ">";
				var title = toDo.decodeHTMLEntities(items.itm.title);
				output += "<td scope=\"row\"><a href=\"" + toDo.URLbase + "node/" + items.itm.nid + "\">" + toDo.decodeHTMLEntities(items.itm.title) + "</a></td>";
				if(Drupal.settings.toDo.displayDeadlineColumn)
				{
					output += "<td>" + items.itm.deadline + "</td>";
				}
				if(Drupal.settings.toDo.displayPriorityColumn)
				{
					output += "<td>" + items.itm.item_status + "</td>";
				}
				output += "</tr>";
			}
			else
			{
				output += "<tr><td";
				switch(true)
				{
					case(Number(Drupal.settings.toDo.displayDeadlineColumn) == 1 && Number(Drupal.settings.toDo.displayPriorityColumn) == 1):
						output += " colspan=\"3\"";
						break;
					case(Number(Drupal.settings.toDo.displayDeadlineColumn) == 1 || Number(Drupal.settings.toDo.displayPriorityColumn) == 1):
						output += " colspan=\"2\"";
						break;
				}
				output += " style=\"text-align:center\">" + Drupal.t("There are no items to display") + "</td></tr>";
			}
		});
		output += '</tbody>';
		if(priorityExists)
		{
			var priorityMessage = "<p><em>" + Drupal.t("Items in red are 'immediate'.") + "</em></p>";
		}
		$("#block-to_do-0 div.content").fadeOut("normal", function()
		{
			if($("#block-to_do-0 p").length && !priorityExists)
			{
				$("#block-to_do-0 p").remove();
			}
			if(!$("#block-to_do-0 p").length && priorityExists)
			{
				$("#block-to_do-0 table").after(priorityMessage);
			}
			if(tabs && changeTabClass)
			{
				$("#block-to_do-0 ul.primary li").toggleClass("active");
			}
			$("#block-to_do-0 div table tbody").replaceWith(output);
			$("#to_do_pagination").replaceWith("<div id=\"to_do_pagination\">" + toDo.pagination(currentPage, pageCount, curType) + "</div>");
			toDo.setPaginationHandlers();
		}).fadeIn("normal");
	});
}

Drupal.behaviors.toDo = function ()
{
	toDo.URLbase = (Drupal.settings.toDo.cleanURLS) ? Drupal.settings.basePath : Drupal.settings.basePath + "?q=";
	immediatePageCount = Drupal.settings.toDo.immediatePageCount;
	allPageCount = Drupal.settings.toDo.allPageCount;
	curType = Drupal.settings.toDo.type;
	curPage = Drupal.settings.toDo.page;
	pageCount = (curType == "immediate") ? immediatePageCount : allPageCount;
	if(Drupal.settings.toDo.displayTabs && !$("#block-to_do-0 ul.primary").length)
	{
		var lis = (curType == "immediate") ? "<li class=\"active\"><a href=\"" + toDo.URLbase + "to_do/json/immediate/1\">" + Drupal.t("Urgent") + "</a></li><li><a href=\"" + toDo.URLbase + "to_do/json/all/1\">" + Drupal.t("All") + "</a></li>" : "<li><a href=\"" + toDo.URLbase + "to_do/json/immediate/1\">" + Drupal.t("Urgent") + "</a></li><li class=\"active\"><a href=\"" + toDo.URLbase + "to_do/json/all/1\">" + Drupal.t("All") + "</a></li>" 
		$("#block-to_do-0 .content").prepend("<ul class=\"primary\">" + lis + "</ul>");
		toDo.setTabHandlers();
	}
	if(!$("#to_do_pagination").length)
	{
		if($("#to_do_list_link").length)
		{
			$("#to_do_list_link").before("<div id=\"to_do_pagination\"></div>");
		}
		else
		{
			$("#block-to_do-0 .content").append("<div id=\"to_do_pagination\"></div>");
		}
		$("#to_do_pagination").append(toDo.pagination(curPage, pageCount, curType));
		toDo.setPaginationHandlers();		
	}
};

<?php

/**
 * callback function for following paths:
 *   to_do
 *   to_do/list
 */
 
function to_do_list($uid)
{
	$list_length = variable_get('to_do_number_of_listings', 10);
	$path = drupal_get_path('module', 'to_do');
	drupal_add_css($path . '/css/to_do_list.css');
	$listings_query = db_rewrite_sql
	(
		'SELECT n.nid, n.title, td.item_status, td.priority, UNIX_TIMESTAMP(td.start_date) as start_date, UNIX_TIMESTAMP(td.deadline) as deadline, UNIX_TIMESTAMP(td.date_finished) as date_finished ' .
		'FROM {node} AS n ' .
		'JOIN {to_do} AS td ' .
		'ON n.vid = td.vid ' .
		'JOIN {to_do_assigned_users} AS tdau ' .
		'ON tdau.vid = n.vid ' .
		'WHERE tdau.uid =  %d'
	);
	$header = array
	(
		array('data' => t('Title'), 'field' => 'title'),
		array('data' => t('Status'), 'field' => 'item_status'),
		array('data' => t('Priority'), 'field' => 'priority', 'sort' => 'desc'),
		array('data' => t('Start Date'), 'field' => 'start_date'),
		array('data' => t('Deadline'), 'field' => 'deadline', 'sort' => 'desc'),
		array('data' => t('Date Finished'), 'field' => 'deadline'),
	);
	$listings_query .= tablesort_sql($header);
	$to_do_listings = pager_query($listings_query, $list_length, 0, NULL, $uid);
	$priority_list = _to_do_priority_list();
	$immediate = _to_do_get_key('priority', t('Immediate'));
	$status_list = _to_do_status_list();
	$date_format = variable_get('to_do_date_format', 'Y/n/j');
	$immediate_exists = FALSE;
	$rows = array();
	if(db_affected_rows($to_do_listings))
	{
		while($listing = db_fetch_array($to_do_listings))
		{
			$row = array();
			$cell = array();
			$row[] = array('data' => l($listing['title'], 'node/' . $listing['nid']), 'scope' => 'row');
			$row[] = $status_list[$listing['item_status']];
			$row[] = $priority_list[$listing['priority']];
			$row[] = ($listing['start_date'] != '') ? format_date($listing['start_date'], 'custom', $date_format) : '---';
			$row[] = ($listing['deadline'] != '') ? format_date($listing['deadline'], 'custom', $date_format) : '---';
			$row[] = ($listing['date_finished'] != '') ? format_date($listing['date_finished'], 'custom', $date_format) : '---';
			if($listing['priority'] == $immediate && $listing['item_status'] != _to_do_get_key('status', t('Finished')))
			{
				$immediate_exists = TRUE;
				$rows[] = array('data' => $row, 'class' => 'immediate');
			}
			else
			{
				$rows[] = $row;
			}
		}
	}
	else
	{
		$row[] = array('data' => t('You currently have no to do list items assigned to you.'), 'colspan' => 6, 'style' => 'text-align:center');
		$rows[] = $row;
	}
	
	$output = theme('table', $header, $rows);
	$output .= theme('pager', array('<<', '<', '', '>', '>>'), $list_length);
	return '<div id="to_do_list">' . $output . '</div>';
}

/**
 * Callback function for to_to/created
 */
 
function to_do_list_created($uid)
{
	$list_length = variable_get('to_do_number_of_listings', 10);
	$path = drupal_get_path('module', 'to_do');
	drupal_add_css($path . '/css/to_do_list.css');

	$listings_query = 
		'SELECT n.nid, n.vid, n.title, td.item_status, td.priority, UNIX_TIMESTAMP(td.start_date) as start_date, UNIX_TIMESTAMP(td.deadline) as deadline, UNIX_TIMESTAMP(td.date_finished) as date_finished, GROUP_CONCAT(u.name) AS names, GROUP_CONCAT(u.uid) AS uids ' .
		'FROM ' .
		'(' .
		  	'SELECT nid, vid, title ' .
			'FROM {node} ' .
			'WHERE uid = %d' .
		') AS n ' .
		'JOIN {to_do} AS td ' .
		'ON n.vid = td.vid ' .
		'JOIN {to_do_assigned_users} AS tdau ' .
		'ON tdau.vid = n.vid ' .
		'JOIN {users} AS u ' .
		'ON tdau.uid = u.uid ' .
		'GROUP BY n.vid';
	$count_query = 'SELECT COUNT(nid) FROM {node} WHERE type="to_do" AND uid = %d';
	$header = array
	(
		array('data' => t('Title'), 'field' => 'title'),
		array('data' => t('Assigned To')),
		array('data' => t('Status'), 'field' => 'item_status'),
		array('data' => t('Priority'), 'field' => 'priority', 'sort' => 'desc'),
		array('data' => t('Start Date'), 'field' => 'start_date'),
		array('data' => t('Deadline'), 'field' => 'deadline'),
		array('data' => t('Date Finished'), 'field' => 'deadline'),
	);
	$listings_query .= tablesort_sql($header);
	$to_do_listings = pager_query($listings_query, $list_length, 0, $count_query, $uid);
	$immediate = _to_do_get_key('priority', t('Immediate'));
	$status_list = _to_do_status_list();
	$priority_list = _to_do_priority_list();
	$date_format = variable_get('to_do_date_format', 'Y/n/j');
	$immediate_exists = FALSE;
	$rows = array();
	if(db_affected_rows($to_do_listings))
	{
		while($list_item = db_fetch_array($to_do_listings))
		{
			$row = array();
			$cell = array();
			$row[] = array('data' => l($list_item['title'], 'node/' . $list_item['nid']), 'scope' => 'row');
			$names = explode(',',$list_item['names']);
			$uids = explode(',', $list_item['uids']);
			$cell = '<ul>';
			for($i = 0; $i < count($names); $i++)
			{
				$name = ($uids[$i] == $uid) ? '<em>' . $names[$i] . '</em>' : $names[$i];
				$cell .= '<li>' . l($name, 'user/' . $uids[$i], array('html' => TRUE)) . '</li>';
			}
			$cell .= '</ul>';
			$row[] = $cell;
			$row[] = $status_list[$list_item['item_status']];
			$row[] = $priority_list[$list_item['priority']];
			$row[] = ($list_item['start_date'] != '') ? format_date($list_item['start_date'], 'custom', $date_format) : '---';
			$row[] = ($list_item['deadline'] != '') ? format_date($list_item['deadline'], 'custom', $date_format) : '---';
			$row[] = ($list_item['date_finished'] != '') ? format_date($list_item['date_finished'], 'custom', $date_format) : '---';
			if($list_item['priority'] == $immediate && $list_item['item_status'] != _to_do_get_key('status', t('Finished')))
			{
				$immediate_exists = TRUE;
				$rows[] = array('data' => $row, 'class' => 'immediate');
			}
			else
			{
				$rows[] = $row;
			}
		}
	}
	else
	{
		$row[] = array('data' => t('You have not created any to do list items.'), 'colspan' => 7, 'style' => 'text-align:center');
		$rows[] = $row;
	}
	$output = theme('table', $header, $rows);
	$output .= theme('pager', array('<<', '<', '', '>', '>>'), $list_length);
	return '<div id="to_do_list">' . $output . '</div>';
}

/**
 * Callback function for to_do/all
 */

function to_do_list_all($uid)
{
	$list_length = variable_get('to_do_number_of_listings', 10);
	$path = drupal_get_path('module', 'to_do');
	drupal_add_css($path . '/css/to_do_list.css');

	$listings_query = 'SELECT n.nid, n.vid, n.title, td.item_status, td.priority, UNIX_TIMESTAMP(td.start_date) as start_date, UNIX_TIMESTAMP(td.deadline) as deadline, UNIX_TIMESTAMP(td.date_finished) as date_finished, GROUP_CONCAT(u.name) AS names, GROUP_CONCAT(u.uid) AS uids ' .
		'FROM {node} AS n ' .
		'JOIN {to_do} AS td ' .
		'ON n.vid = td.vid ' .
		'JOIN {to_do_assigned_users} AS tdau ' .
		'ON tdau.vid = n.vid ' .
		'JOIN {users} AS u ' .
		'ON tdau.uid = u.uid ' .
		'GROUP BY n.vid';
	$count_query = 'SELECT COUNT(nid) FROM {node} WHERE type = "to_do"';

	$header = array
	(
		array('data' => t('Title'), 'field' => 'title'),
		array('data' => t('Assigned To')),
		array('data' => t('Status'), 'field' => 'item_status'),
		array('data' => t('Priority'), 'field' => 'priority', 'sort' => 'desc'),
		array('data' => t('Start Date'), 'field' => 'start_date'),
		array('data' => t('Deadline'), 'field' => 'deadline'),
		array('data' => t('Date Finished'), 'field' => 'deadline'),
	);
	$listings_query .= tablesort_sql($header);
	$to_do_listings = pager_query($listings_query, $list_length, 0, $count_query);
	$immediate = _to_do_get_key('priority', t('Immediate'));
	$status_list = _to_do_status_list();
	$priority_list = _to_do_priority_list();
	$date_format = variable_get('to_do_date_format', 'Y/n/j');
	$immediate_exists = FALSE;
	$rows = array();
	while($list_item = db_fetch_array($to_do_listings))
	{
		$row = array();
		$cell = array();
		$row[] = array('data' => l($list_item['title'], 'node/' . $list_item['nid']), 'scope' => 'row');
		$names = explode(',',$list_item['names']);
		$uids = explode(',', $list_item['uids']);
		$cell = '<ul>';
		for($i = 0; $i < count($names); $i++)
		{
			$name = ($uids[$i] == $uid) ? '<em>' . $names[$i] . '</em>' : $names[$i];
			$cell .= '<li>' . l($name, 'user/' . $uids[$i], array('html' => TRUE)) . '</li>';
		}
		$cell .= '</ul>';
		$row[] = $cell;
		$row[] = $status_list[$list_item['item_status']];
		$row[] = $priority_list[$list_item['priority']];
		$row[] = ($list_item['start_date'] != '') ? format_date($list_item['start_date'], 'custom', $date_format) : '---';
		$row[] = ($list_item['deadline'] != '') ? format_date($list_item['deadline'], 'custom', $date_format) : '---';
		$row[] = ($list_item['date_finished'] != '') ? format_date($list_item['date_finished'], 'custom', $date_format) : '---';
		if($list_item['priority'] == $immediate && $list_item['item_status'] != _to_do_get_key('status', t('Finished')))
		{
			$immediate_exists = TRUE;
			$rows[] = array('data' => $row, 'class' => 'immediate');
		}
		else
		{
			$rows[] = $row;
		}
	}
	$output = theme('table', $header, $rows);
	$output .= theme('pager', array('<<', '<', '', '>', '>>'), $list_length);
	return '<div id="to_do_list">' . $output . '</div>';
}



/**
 * Callback function for admin/settings/to_do
 */
 
function to_do_settings($form_state)
{
	ahah_helper_register($form, $form_state);
	
	$form['block_settings'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('Block Settings'),
	);
	$form['block_settings']['to_do_deadline_column_display'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Display deadline column'),
		'#description' => t('Unchecking this box will remove the deadline column from the block provided by this module.'),
		'#default_value' => variable_get('to_do_deadline_column_display', 1),
	);
	$form['block_settings']['to_do_priority_column_display'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Display status column'),
		'#description' => t('Unchecking this box will remove the status column from the block provided by this module.'),
		'#default_value' => variable_get('to_do_priority_column_display', 1),
	);
	$form['block_settings']['to_do_display_icon'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Display icon'),
		'#description' => t('Unchecking this box will remove the icon that links to the to_do/list page from the block provided by this module.'),
		'#default_value' => variable_get('to_do_display_icon', 1),
	);
	$form['block_settings']['to_do_list_tabs_display_wrapper'] = array
	(
		'#prefix' => '<div id="tabs_display_wrapper">',
		'#suffix' => '</div>',
	);
	$form['block_settings']['to_do_list_tabs_display_wrapper']['to_do_list_tabs_display'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Display tabs'),
		'#description' => t('Unchecking this box will remove the tabs from the block provided by this module.<br />Note: If you change this value, you must save the settings before your selection will be reflected on the site.'),
		'#default_value' => (isset($form_state['values']['to_do_list_tabs_display'])) ?  $form_state['values']['to_do_list_tabs_display'] : variable_get('to_do_list_tabs_display', 1),
		'#ahah' => array
		(
			'path' => ahah_helper_path(array('block_settings', 'to_do_list_tabs_display_wrapper')),
			'wrapper' => 'tabs_display_wrapper',
			'method' => 'replace',
			'effect' => 'fade',
		),
	);
	if((isset($form_state['values']['to_do_list_tabs_display']) && !$form_state['values']['to_do_list_tabs_display']) || (!isset($form_state['values']['to_do_list_tabs_display']) && !variable_get('to_do_list_tabs_display', 1)))
	{
		$single_selection_default = (isset($form_state['values']['to_do_list_tabs_display_single_selection'])) ? $form_state['values']['to_do_list_tabs_display_single_selection'] : variable_get('to_do_list_tabs_display_single_selection', NULL);
		$form['block_settings']['to_do_list_tabs_display_wrapper']['to_do_list_tabs_display_single_selection'] = array
		(
			'#type' => 'radios',
			'#title' => t('List to display'),
			'#description' => t('Choose which items you would like to display in the block'),
			'#options' => _to_do_single_column_list(),
			'#required' => TRUE,
			'#default_value' => $single_selection_default,
		);
	}
	$form['to_do_date_format'] = array
	(
		'#type' => 'textfield',
		'#title' => t('Date Format'),
		'#description' => t("Enter the format to be used for dates displayed by the To Do List module. Use a !php_date format string.<br />Note: No hours, minutes, or seconds are provided with dates in this module, so don't include these in your format string.", array('!php_date' => '<a href="http://www.php.net/date">' . t('PHP date') . '</a>')),
		'#default_value' => variable_get('to_do_date_format', 'Y/n/j'),
	);
	$form['to_do_number_of_listings'] = array
	(
		'#type' => 'textfield',
		'#title' => t('Number of listings to show on listings pages'),
		'#description' => t('Enter the number of listings to be displayed on pages that display lists of to do listings. This number is independent from block settings, which are set on a per/user basis in their control panel'),
		'#default_value' => variable_get('to_do_number_of_listings', 10),
	);
	return system_settings_form($form);
}



function to_do_settings_validate($form, &$form_state)
{
	$_SESSION['to_do_page'] = 1;
	if(!preg_match('/^[0-9]+$/', $form_state['values']['to_do_number_of_listings']))
	{
		form_set_error('to_do_number_of_listings', t('The number of listings must be a number.'));
	}
}

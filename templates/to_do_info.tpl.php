<?php
?>
<dl>
	<dt><?php print t("Status"); ?></dt>
	<dd><?php print $item_status ?></dd>

	<dt><?php print t("Priority"); ?></dt>
	<dd><?php print $priority ?></dd>

	<?php if($start_date): ?>
		<dt><?php print t("Start Date"); ?></dt>
		<dd><?php print $start_date ?></dd>
	<?php endif; ?>

	<?php if($deadline): ?>
		<dt><?php print t("Deadline"); ?></dt>
		<dd><?php print $deadline ?></dd>
	<?php endif; ?>
	
	<?php if($users): ?>
		<dt><?php print t("Assigned To"); ?></dt>
		<dd><ul><?php foreach($users as $u) { ?><li><?php print $u ?></li><?php } ?></ul></dd>
	<?php endif; ?>
</dl>
<?php if($finished_button) {print ($finished_button);} ?>
